from django.contrib import admin

from .models import *

admin.site.register(Departamento)
admin.site.register(Ciudad)
admin.site.register(Evento)
admin.site.register(Organizador)
