#!/usr/bin/python
# -*- coding: utf-8 -*-



from django.db import models
from django.contrib.auth.models import User


class Departamento (models.Model):
	nombre = models.CharField(max_length=30)

	def __str__(self):
		return self.nombre


class Ciudad (models.Model):
	nombre = models.CharField(max_length=30)
	departamento = models.ForeignKey(Departamento)

	def __str__(self):
		return self.nombre


class Evento (models.Model):
	nombre = models.CharField(max_length=30)
	descripcion = models.CharField(max_length=300)
	fecha = models.DateField()
	hora = models.TimeField()
	lugar = models.CharField(max_length=30)
	ciudad = models.ForeignKey(Ciudad)
	usuario = models.ForeignKey(User)
	imagen =models.FileField(upload_to='media/', blank=True, null=True)
	#imagen =models.ImageField(upload_to='images/', blank=True, null=True)
	def __str__(self):
		return self.nombre


class Organizador (models.Model):
	nombre = models.CharField(max_length=30)
	email = models.EmailField()
	ciudad = models.ForeignKey(Ciudad)
	evento = models.ForeignKey(Evento)

	def __str__(self):
		return self.nombre
