#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.template import Context
from django.template.loader import get_template
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.template.context import RequestContext
from django.contrib.auth.models import User
from django.utils import timezone
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DeleteView, TemplateView
# from .forms import FormularioEvento
from django.core.urlresolvers import reverse_lazy
from .models import *
# Buscador

from REGISTRO.models import Evento
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt


@login_required()
def home_auth(request):
	eventos = Evento.objects.order_by('-fecha')[:15]
	context = {
		'eventos':eventos
	}
	return render_to_response('home_logout.html',context)

#buscador
@csrf_exempt
def busqueda(request):
   q = request.GET.get('q', '')
   eventos = Evento.objects.filter(ciudad__nombre__icontains=q)
   return render(request, 'home.html', {'eventos': eventos})

class CreateEventView(TemplateView):
	"""Vista de creacion de eventos."""
	template_name = 'evento_form.html'
	def get(self, request, *args, **kwargs):
		ciudades = Ciudad.objects.all()
		return render(request,self.template_name,{'data': ciudades })
	def post(self, request):
		print(request.FILES['imagen'])
		Evento.objects.create(nombre=request.POST['nombre'],descripcion=request.POST['desc'],
			fecha=request.POST['fecha'],hora=request.POST['hora'],lugar=request.POST['lugar'],
			ciudad_id=int(request.POST['ciudad']),usuario=request.user,imagen=request.FILES['imagen'])
		return redirect('listar_eventos')

# class UpdateEventView(UpdateView):
# 	"""Vista para editar eventos."""

# 	model = Evento
# 	template_name = 'evento_form.html'
# 	form_class = FormularioEvento
# 	success_url = reverse_lazy('editar_evento', args=(0, ))

# 	def form_valid(self, form):
# 		event = form.instance
# 		self.success_url = reverse_lazy('editar_evento', args=(event.pk, ))
# 		return super().form_valid(form)


class ListEventView(ListView):
	"""Vista para listar los eventos."""

	model = Evento
	template_name = 'event_list.html'


def eliminar_evento(request, pk):
	"""Vista para eliminar los eventos."""

	evento = get_object_or_404(Evento, pk=pk)
	evento.delete()
	return redirect('listar_eventos')
