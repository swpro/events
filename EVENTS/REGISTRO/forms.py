from django import forms
from .models import Evento


class FormularioEvento(forms.ModelForm):
	"""Formulario para crear eventos."""

	css_error_class = 'has-error'

	class Meta:
		model = Evento
		fields = ('nombre', 'descripcion', 'fecha', 'hora', 'lugar', 'ciudad', 'imagen')

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		for field in self.fields:
			self.fields[field].widget.attrs.update({'class': 'form-control'})