"""EVENTS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.contrib.auth.views import login, logout
from django.contrib.auth.decorators import login_required

from REGISTRO import views

from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^busqueda', views.busqueda),
    
    #/*SISTEMA DE LOGIN COMPLETO*/
    url(r'^login/$', login, {'template_name': 'index.html', }, name="login"),
    url(r'^logout/$', logout, {'template_name': 'logout.html', }),
    url(r'^signup/$', 'HOME.views.signup', name='signup'),
    #/*FIN*/

    #/*SISTEMA PARTE DEL INICIO*/
    url(r'^$', 'HOME.views.home', name='home'),
    url(r'^home/$', 'REGISTRO.views.home_auth', name='home_auth'),
    url(r'^eventos/nuevo/$', views.CreateEventView.as_view(), name='crear_evento'),
    # url(r'^eventos/(?P<pk>\d+)/editar/$', views.UpdateEventView.as_view(), name='editar_evento'),
    url(r'^eventos/$', views.ListEventView.as_view(), name='listar_eventos'),
    url(r'^eventos/(?P<pk>\d+)/eliminar/$', views.eliminar_evento, name='eliminar_evento'),
    #/*FIN*/

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
